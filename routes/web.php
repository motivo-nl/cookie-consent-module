<?php

use Motivo\CookieConsent\Http\Controllers\CookieConsentController;

Route::post('/cookie-consent', CookieConsentController::class)
    ->name('cookie_consent');
