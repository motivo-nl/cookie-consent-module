<?php

namespace Motivo\CookieConsent;

use Illuminate\Support\ServiceProvider;
use Motivo\CookieConsent\Providers\RouteServiceProvider;

class CookieConsentServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        /** @deprecated  */
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'cc');

        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'cookieConsent');

        $this->mergeConfigFrom(__DIR__ . '/../config/cookie_consent.php', 'cookie_consent');

        $this->publishes([
            __DIR__ . '/../config/cookie_consent.php' => config_path('cookie_consent.php'),
        ], ['cookie-consent-config']);

        $this->publishes([
            __DIR__ . '/../resources/views' => resource_path('views/vendor/cookieConsent'),
        ], ['cookie-consent-views']);
    }

    public function register(): void
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
