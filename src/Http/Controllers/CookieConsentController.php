<?php

namespace Motivo\CookieConsent\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Motivo\CookieConsent\CookieConsent;

class CookieConsentController
{
    public function __invoke(Request $request): RedirectResponse
    {
        switch ($request->input('cookie-consent-type')) {
            case '3':
                $types = [
                    CookieConsent::TRACKING,
                    CookieConsent::ANALYSIS,
                    CookieConsent::FUNCTIONAL,
                ];

                break;
            case '2':
                $types = [
                    CookieConsent::ANALYSIS,
                    CookieConsent::FUNCTIONAL,
                ];

                break;
            default:
                $types = [
                    CookieConsent::FUNCTIONAL,
                ];

                break;
        }

        setcookie(CookieConsent::cookieName(), json_encode($types), Carbon::now()->addMinutes(config('cookie_consent.cookie_lifetime'))->unix());

        return redirect()->back();
    }
}
