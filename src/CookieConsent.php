<?php

namespace Motivo\CookieConsent;

class CookieConsent
{
    public const FUNCTIONAL = 'functional';

    public const ANALYSIS = 'analysis';

    public const TRACKING = 'tracking';

    public function isFunctional(): bool
    {
        if (! $this->hasConsent()) {
            return false;
        }

        return in_array(static::FUNCTIONAL, $this->getConsent());
    }

    public function isAnalysis(): bool
    {
        if (! $this->hasConsent()) {
            return false;
        }

        return in_array(static::ANALYSIS, $this->getConsent());
    }

    public function isTracking(): bool
    {
        if (! $this->hasConsent()) {
            return false;
        }

        return in_array(static::TRACKING, $this->getConsent());
    }

    public function getConsent(): array
    {
        return json_decode($_COOKIE[static::cookieName()], true) ?? [];
    }

    public function hasConsent(): bool
    {
        return isset($_COOKIE[static::cookieName()]);
    }

    public static function cookieName(): string
    {
        return config('cookie_consent.cookie_name');
    }
}
