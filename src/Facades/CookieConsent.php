<?php

namespace Motivo\CookieConsent\Facades;

use Illuminate\Support\Facades\Facade;

class CookieConsent extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return \Motivo\CookieConsent\CookieConsent::class;
    }
}
