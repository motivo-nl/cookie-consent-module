# Cookie consent with multiple levels

Add a cookie consent to your website with the levels of 'Functional', 'Analysis', 'Tracking'.

## Installation

Install this package using composer:
```
composer require motivo/cookie-consent
```

Auto-discovery is enabled, so the package will register itself with Laravel.

### Config

This package comes with a config file, to publish this file run the following command:
```
php artisan vendor:publish --provider="Motivo\CookieConsent\CookieConsentServiceProvider"
```

## Usage

The package offers a blade file which you can include with `@include('cookieConsent::cookie_consent')`.
Add this file where you want to show the notification. Preferable somewhere in the root of you application.

To check if the user has consented and to what level you can use class `\Motivo\CookieConsent\CookieConsent` or the facade `\CookieConsent`.

The following methods are available:

* `\CookieConsent::isFunctional()` - Check if the user has consented to functional
* `\CookieConsent::isAnalysis()` - Check if the user has consented to analysis
* `\CookieConsent::isTracking()` - Check if the user has consented to tracking
* `\CookieConsent::getConsent()` - Get an array of what the user has consented to
* `\CookieConsent::hasConsent()` - Check if the user has consented


The package will automatically register a route to store the choise of the user in a cookie. The cookie will be stored for 1 year.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

## Credits

- [Motivo Zwolle](https://bitbucket.org/motivo-nl/)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.