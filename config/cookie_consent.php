<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Cookie consent page
    |--------------------------------------------------------------------------
    |
    | Most websites provide a page which explains all cookies used on the website.
    | Using this config will add a link within the notification to the specified page.
    |
    */
    'cookie_page' => null,

    /*
    |--------------------------------------------------------------------------
    | Website name
    |--------------------------------------------------------------------------
    |
    | The cookie notification has a default text, the website name will be pre-filled.
    |
    */
    'website_name' => env('COOKIE_CONSENT_WEBSITE_NAME', config('app.name')),

    /*
    |--------------------------------------------------------------------------
    | Cookie name
    |--------------------------------------------------------------------------
    |
    | The string value of the name of the cookie.
    |
    */
    'cookie_name' => 'cookie_consent',

    /*
    |--------------------------------------------------------------------------
    | Cookie lifetime
    |--------------------------------------------------------------------------
    |
    | The lifetime of the consent cookie.
    | The time is in minutes, so the default is a year.
    |
    */
    'cookie_lifetime' => 60 * 24 * 365,
];