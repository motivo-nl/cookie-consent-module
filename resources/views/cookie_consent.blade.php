@if (! \CookieConsent::hasConsent())
    <div class="cookie-consent">
        <form method="post" action="{{ route('cookie_consent') }}">
            @csrf
            <div class="cc-top">
                <div class="cc-top-group">
                    <div class="cc-top-content">
                        <h3 class="cc-top-title">
                            Wij gebruiken cookies
                        </h3>
                        <p class="cc-top-text">
                            Door op 'Akkoord' te klikken, accepteer je alle cookies (en vergelijkbare technieken) op onze website.
                            Hiermee kunnen wij en derden jouw internetgedrag op onze website volgen en verwerken.
                            Door ons en derden kan een profiel worden gemaakt waarmee gepersonaliseerde advertenties kunnen worden getoond.
                            Door op 'instellingen wijzigen' te klikken, kun je een andere cookie voorkeur kiezen.
                        </p>
                    </div>
                    <div class="cc-top-buttons">
                        <button class="cc-button submit" type="submit">Akkoord</button>
                        <button class="cc-button" type="button" id="js-cookie-settings-button">Instellingen wijzigen</button>
                    </div>
                </div>
                @if (config('cookie_consent.cookie_page'))
                    <a class="cc-button" href="{{ config('cookie_consent.cookie_page') }}">Meer informatie over de cookies</a>
                @endif
            </div>

            <div class="cc-settings" id="js-cookie-settings-container" style="display: none;">
                <div class="cc-setting-option js-cookie-setting-button">
                    <div class="cc-setting-stars">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" style="width: 30px; fill: #6d9e55;" data-filled="true">
                            <path d="M10 1.3l2.388 6.722H18.8l-5.232 3.948 1.871 6.928L10 14.744l-5.438 4.154 1.87-6.928-5.233-3.948h6.412L10 1.3z"/>
                        </svg>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" style="width: 30px; fill: #6d9e55;" data-filled="true">
                            <path d="M10 1.3l2.388 6.722H18.8l-5.232 3.948 1.871 6.928L10 14.744l-5.438 4.154 1.87-6.928-5.233-3.948h6.412L10 1.3z"/>
                        </svg>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" style="width: 30px; fill: #6d9e55;" data-filled="true">
                            <path d="M10 1.3l2.388 6.722H18.8l-5.232 3.948 1.871 6.928L10 14.744l-5.438 4.154 1.87-6.928-5.233-3.948h6.412L10 1.3z"/>
                        </svg>

                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" style="width: 30px; fill: #777777;" data-empty="true">
                            <path d="M18.8 8.022h-6.413L10 1.3 7.611 8.022H1.199l5.232 3.947-1.871 6.929L10 14.744l5.438 4.154-1.869-6.929L18.8 8.022zM10 12.783l-3.014 2.5 1.243-3.562-2.851-2.3 3.522.101 1.1-4.04 1.099 4.04 3.521-.101-2.851 2.3 1.243 3.562-3.012-2.5z"/>
                        </svg>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" style="width: 30px; fill: #777777;" data-empty="true">
                            <path d="M18.8 8.022h-6.413L10 1.3 7.611 8.022H1.199l5.232 3.947-1.871 6.929L10 14.744l5.438 4.154-1.869-6.929L18.8 8.022zM10 12.783l-3.014 2.5 1.243-3.562-2.851-2.3 3.522.101 1.1-4.04 1.099 4.04 3.521-.101-2.851 2.3 1.243 3.562-3.012-2.5z"/>
                        </svg>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" style="width: 30px; fill: #777777;" data-empty="true">
                            <path d="M18.8 8.022h-6.413L10 1.3 7.611 8.022H1.199l5.232 3.947-1.871 6.929L10 14.744l5.438 4.154-1.869-6.929L18.8 8.022zM10 12.783l-3.014 2.5 1.243-3.562-2.851-2.3 3.522.101 1.1-4.04 1.099 4.04 3.521-.101-2.851 2.3 1.243 3.562-3.012-2.5z"/>
                        </svg>
                    </div>

                    <p class="cc-setting-text">
                        Functionele cookies die zorgen voor een goed werkende website. Jouw internetgedrag wordt gedeeld met {{ config('cookie_consent.website_name') }} en derden die cookies plaatsen.
                    </p>

                    <input type="radio" name="cookie-consent-type" value="3" class="js-cookie-setting-button-input" style="display: none;" checked>
                </div>

                <div class="cc-setting-option js-cookie-setting-button">
                    <div class="cc-setting-stars">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" style="width: 30px; fill: #6d9e55;" data-filled="true">
                            <path d="M10 1.3l2.388 6.722H18.8l-5.232 3.948 1.871 6.928L10 14.744l-5.438 4.154 1.87-6.928-5.233-3.948h6.412L10 1.3z"/>
                        </svg>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" style="width: 30px; fill: #6d9e55;" data-filled="true">
                            <path d="M10 1.3l2.388 6.722H18.8l-5.232 3.948 1.871 6.928L10 14.744l-5.438 4.154 1.87-6.928-5.233-3.948h6.412L10 1.3z"/>
                        </svg>

                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" style="width: 30px; fill: #777777;" data-empty="true">
                            <path d="M18.8 8.022h-6.413L10 1.3 7.611 8.022H1.199l5.232 3.947-1.871 6.929L10 14.744l5.438 4.154-1.869-6.929L18.8 8.022zM10 12.783l-3.014 2.5 1.243-3.562-2.851-2.3 3.522.101 1.1-4.04 1.099 4.04 3.521-.101-2.851 2.3 1.243 3.562-3.012-2.5z"/>
                        </svg>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" style="width: 30px; fill: #777777;" data-empty="true">
                            <path d="M18.8 8.022h-6.413L10 1.3 7.611 8.022H1.199l5.232 3.947-1.871 6.929L10 14.744l5.438 4.154-1.869-6.929L18.8 8.022zM10 12.783l-3.014 2.5 1.243-3.562-2.851-2.3 3.522.101 1.1-4.04 1.099 4.04 3.521-.101-2.851 2.3 1.243 3.562-3.012-2.5z"/>
                        </svg>
                    </div>

                    <p class="cc-setting-text">
                        Functionele cookies die zorgen voor een goed werkende website. Jouw internetgedrag wordt gedeeld met {{ config('cookie_consent.website_name') }}.
                    </p>

                    <input type="radio" name="cookie-consent-type" value="2" class="js-cookie-setting-button-input" style="display: none;">
                </div>

                <div class="cc-setting-option js-cookie-setting-button">
                    <div class="cc-setting-stars">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" style="width: 30px; fill: #6d9e55;" data-filled="true">
                            <path d="M10 1.3l2.388 6.722H18.8l-5.232 3.948 1.871 6.928L10 14.744l-5.438 4.154 1.87-6.928-5.233-3.948h6.412L10 1.3z"/>
                        </svg>

                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" style="width: 30px; fill: #777777;" data-empty="true">
                            <path d="M18.8 8.022h-6.413L10 1.3 7.611 8.022H1.199l5.232 3.947-1.871 6.929L10 14.744l5.438 4.154-1.869-6.929L18.8 8.022zM10 12.783l-3.014 2.5 1.243-3.562-2.851-2.3 3.522.101 1.1-4.04 1.099 4.04 3.521-.101-2.851 2.3 1.243 3.562-3.012-2.5z"/>
                        </svg>
                    </div>

                    <p class="cc-setting-text">
                        Functionele cookies die zorgen voor een goed werkende website.
                    </p>

                    <input type="radio" name="cookie-consent-type" value="1" class="js-cookie-setting-button-input" style="display: none;">
                </div>

            </div>
        </form>
    </div>

    <script>
        document.addEventListener('DOMContentLoaded', (event) => {
            let container = document.getElementById('js-cookie-settings-container');

            document.getElementById('js-cookie-settings-button').addEventListener('click', () => {
                container.style.display = container.style.display === 'none' ? 'flex' : 'none'
            })

            const radioButtons = document.getElementsByClassName('js-cookie-setting-button');

            for (let i = 0; i < radioButtons.length; i++) {
                let button = radioButtons[i]
                button.addEventListener('click', (event) => {

                    button.getElementsByClassName('js-cookie-setting-button-input')[0].checked = true
                    for (let j = 0; j < radioButtons.length; j++) {
                        radioButtons[j].classList.remove('cc-setting-selected')
                    }
                    button.classList.add('cc-setting-selected')
                })

                if (button.getElementsByClassName('js-cookie-setting-button-input')[0].checked) {
                    button.classList.add('cc-setting-selected')
                }
            }
        })
    </script>

    <style>
        .cookie-consent {
            position: fixed;
            background: white;
            max-height: 100%;
            width: 100%;
            bottom: 0;
            box-shadow: 0 -2px 6px rgba(0,0,0, .1);
            overflow-y: auto;
            z-index: 999;
        }

        .cc-top-title,
        .cc-top-text,
        .cc-setting-text,
        .cc-button {
            font-family: sans-serif;
            font-weight: normal;
            color: #333;
            line-height: 1.5em;
        }

        .cc-top {
            padding: 20px;
        }

        .cc-top-group {
            display: flex;
            flex-flow: column wrap;
        }

        .cc-top-title {
            font-weight: bold;
        }

        .cc-top-content {
            display: flex;
            flex-flow: column wrap;
            width: 100%;
        }

        .cc-top-text {
            display: none;
        }

        .cc-top-buttons {
            display: flex;
            flex-flow: row wrap;
            align-items: center;
            width: 100%;
        }

        .cc-top-buttons .cc-button {
            width: 100%;
        }

        .cc-top-buttons .cc-button:not(:last-child) {
            margin-bottom: 10px;
        }

        .cc-button {
            display: inline-block;
            padding: 12px 20px;
            color: #111;
            font-size: 1rem;
            text-align: center;
            background: transparent;
            border: 1px solid rgba(0,0,0, .1);
            border-radius: 4px;
            cursor: pointer;
        }

        .cc-button.submit {
            font-weight: bold;
            color: white;
            background: #6d9e55;
            border-color: #6d9e55;
        }

        .cc-settings {
            flex-flow: column wrap;
            justify-content: space-between;
            padding: 20px;
            background-color: #f5f5f5;
        }

        .cc-settings svg[data-filled] {
            display: none;
        }

        .cc-setting-option {
            width: 100%;
            padding: 20px;
            border: 1px solid rgba(0,0,0, .1);
            border-radius: 4px;
            cursor: pointer;
        }

        .cc-setting-stars {
            display: flex;
            justify-content: center;
        }

        .cc-setting-text {
            margin-top: 15px;
        }

        .cc-setting-selected {
            border: 1px solid #6d9e55 !important;
        }

        .cc-setting-selected .cc-setting-text {
            color: #6d9e55;
            font-weight: bold;
        }

        .cc-setting-selected svg[data-empty] {
            display: none;
        }

        .cc-setting-selected svg[data-filled] {
            display: block;
        }

        @media only screen and (max-width: 767px) {
            .cc-top-content {
                margin-bottom: 10px;
            }

            .cc-top-buttons {
                padding: 10px 0;
            }

            .cc-button {
                width: 100%;
            }

            .cc-setting-option:not(:last-child) {
                margin-bottom: 10px;
            }
        }

        @media only screen and (min-width: 768px) {
            .cc-top {
                padding: 40px;
            }

            .cc-top-group {
                flex-flow: row nowrap;
                margin-bottom: 20px;
            }

            .cc-top-content {
                width: calc(100% - 200px);
                padding-right: 40px;
            }

            .cc-top-text {
                display: block;
                margin-top: 20px;
            }

            .cc-top-buttons {
                flex-flow: column wrap;
                width: 200px;
            }

            .cc-settings {
                flex-flow: row nowrap;
                justify-content: space-between;
                padding: 40px;
            }

            .cc-setting-option {
                width: calc((100% - 40px) / 3);
            }
        }
    </style>

@endif
